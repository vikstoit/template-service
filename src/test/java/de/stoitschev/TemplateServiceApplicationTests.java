package de.stoitschev;

import de.stoitschev.model.Template;
import de.stoitschev.repository.TemplateRepository;
import de.stoitschev.service.TemplateServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TemplateServiceApplicationTests {

	@Test
	public void contextLoads() {
	    assert true;
	}

	@Test
	public void testTemplate() {

		TemplateRepository repository = mock(TemplateRepository.class);

		Template template = new Template("First_Template");
		template.setId("1");


		when(repository.findByName("First_Template")).thenReturn(template);

		TemplateServiceImpl templateService = new TemplateServiceImpl(repository);



		Template result = templateService.findByName("First_Template");

		Assert.assertEquals("1", result.getId());
		Assert.assertEquals("First_Template", result.getName());
	}
}

