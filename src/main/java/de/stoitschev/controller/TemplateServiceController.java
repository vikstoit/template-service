package de.stoitschev.controller;

import de.stoitschev.model.Template;
import de.stoitschev.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/template-service", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class TemplateServiceController {

    @Autowired
    TemplateService templateService;

    @RequestMapping(path = "/template", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<?> getTemplate(){
//        Template template = templateService.findByName();
        Template template = new Template("First-Template");
        return ResponseEntity.ok()
                .body(template);
    }
}
