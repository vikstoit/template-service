package de.stoitschev.repository;

import de.stoitschev.model.Template;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TemplateRepository extends MongoRepository<Template, String> {
    Template findByName(String name);
}
