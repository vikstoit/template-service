package de.stoitschev.service;

import de.stoitschev.model.Template;
import de.stoitschev.repository.TemplateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public class TemplateServiceImpl implements TemplateService {

    Logger log = LoggerFactory.getLogger(TemplateServiceImpl.class);

    @Autowired
    private TemplateRepository templateRepository;

    public TemplateServiceImpl() {}

    public TemplateServiceImpl(TemplateRepository templateRepository) {
        this.templateRepository = templateRepository;
    }

    @Override
    public Template findByName(String name) {
        return templateRepository.findByName(name);
    }


}
