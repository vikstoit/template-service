package de.stoitschev.service;

import de.stoitschev.model.Template;

import java.util.List;

public interface TemplateService {
   Template findByName(String name);
}
